/*
 * Copyright (C) 2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-cmdexec.
 * 
 * Devinsy-cmdexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-cmdexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-cmdexec.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.cmdexec.demo;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.cmdexec.CmdExec;
import fr.devinsy.util.cmdexec.CmdExecException;
import fr.devinsy.util.cmdexec.CmdExecUtils;
import fr.devinsy.util.cmdexec.StreamGobbler;

/**
 * The Class Demo.
 */
public class CmdExecDemo
{
	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 * @throws CmdExecException
	 * @throws IOException
	 */
	public static void main(final String[] args) throws CmdExecException, IOException
	{
		//
		BasicConfigurator.configure();
		org.apache.log4j.Logger.getRootLogger().setLevel(Level.INFO);
		Logger logger = LoggerFactory.getLogger(CmdExecDemo.class);

		//
		File tmpFile = null;
		try
		{
			tmpFile = File.createTempFile("cmdexec", ".tmp");

			// #0
			{
				System.out.println("========== DEMO #0;");

				System.out.println("Launch…");

				// String result = CmdExecUtils.run("/bin/bash", "-c",
				// "echo '10\n5\n7\n1' > " + tmpFile.getAbsolutePath());
				String result = CmdExecUtils.run("/bin/bash -c \"echo '10\n5\n7\n1' > " + tmpFile.getAbsolutePath() + " \"");

				System.out.println("result=[" + result + "]");
			}

			// #1
			{
				System.out.println("========== DEMO #1;");

				System.out.println("Launch…");

				String[] command = { "/bin/cat", tmpFile.getAbsolutePath() };

				CmdExec cmdexec = new CmdExec(command, StreamGobbler.StreamWay.BUFFER, StreamGobbler.StreamWay.BUFFER);
				System.out.println("command=[" + cmdexec.toString() + "]");
				System.out.println("exitVal=[" + cmdexec.getExitValue() + "]");
				System.out.println("out=[" + cmdexec.getOutStream() + "]");
				System.out.println("err=[" + cmdexec.getErrStream() + "]");
			}

			// #2
			{
				System.out.println("========== DEMO #2;");

				System.out.println("Launch…");

				String[] command = { "/usr/bin/sort", "-n", tmpFile.getAbsolutePath() };

				CmdExec cmdexec = new CmdExec(command, StreamGobbler.StreamWay.BUFFER, StreamGobbler.StreamWay.BUFFER);
				System.out.println("command=[" + cmdexec.toString() + "]");
				System.out.println("exitVal=[" + cmdexec.getExitValue() + "]");
				System.out.println("out=[" + cmdexec.getOutStream() + "]");
				System.out.println("err=[" + cmdexec.getErrStream() + "]");
			}

			// #3
			{
				System.out.println("========== DEMO #3;");

				System.out.println("Launch…");

				String command = "/usr/bin/sort -n -r " + tmpFile.getAbsolutePath();

				CmdExec cmdexec = new CmdExec(command, StreamGobbler.StreamWay.BUFFER, StreamGobbler.StreamWay.BUFFER);
				System.out.println("command=[" + cmdexec.toString() + "]");
				System.out.println("exitVal=[" + cmdexec.getExitValue() + "]");
				System.out.println("out=[" + cmdexec.getOutStream() + "]");
				System.out.println("err=[" + cmdexec.getErrStream() + "]");
			}
		}
		finally
		{
			if (tmpFile != null)
			{
				boolean status = tmpFile.delete();
				logger.debug("file delete status={}", status);
			}
		}
	}
}
