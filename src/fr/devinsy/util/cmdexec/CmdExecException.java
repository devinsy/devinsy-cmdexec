/*
 *  Copyright (C) 2016-2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-cmdexec.
 * 
 * Devinsy-cmdexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-cmdexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-cmdexec.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.cmdexec;

/**
 * The Class CmdExecException.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class CmdExecException extends Exception
{
	private static final long serialVersionUID = 3264886426311529668L;

	/**
	 * Instantiates a new cmd exec exception.
	 */
	public CmdExecException()
	{
		super();
	}

	/**
	 * Instantiates a new cmd exec exception.
	 * 
	 * @param message
	 *            the message
	 */
	public CmdExecException(final String message)
	{
		super(message);
	}

	/**
	 * Instantiates a new cmd exec exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public CmdExecException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * Instantiates a new cmd exec exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public CmdExecException(final Throwable cause)
	{
		super(cause);
	}
}
