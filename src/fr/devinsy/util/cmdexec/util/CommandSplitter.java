/*
 *  Copyright (C) 2005-2010,2013,2015-2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-cmdexec.
 * 
 * Devinsy-cmdexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-cmdexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-cmdexec.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.cmdexec.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.strings.StringList;

/**
 * The Class SplitWorker.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class CommandSplitter
{
	public enum State
	{
		MAIN,
		SIMPLE_QUOTE,
		QUOTE,
		ENDED
	}

	private static Logger logger = LoggerFactory.getLogger(CommandSplitter.class);;

	/**
	 * Split a string command to an array managing quote and simple quote.
	 * 
	 * @param command
	 *            the command
	 * @return the string[]
	 */
	public static String[] split(final String command)
	{
		String[] result;

		if (command == null)
		{
			result = null;
		}
		else
		{
			boolean ended = false;
			State state = State.MAIN;
			StringList tokens = new StringList();
			StringBuilder tokenBuffer = new StringBuilder();
			int letterIndex = 0;
			while (!ended)
			{
				if (letterIndex < command.length())
				{
					char letter = command.charAt(letterIndex);

					switch (state)
					{
						case MAIN:
							switch (letter)
							{
								case ' ':
								{
									//
									String token = tokenBuffer.toString();
									if (!StringUtils.isBlank(token))
									{
										tokens.add(token);
									}

									//
									state = State.MAIN;
									tokenBuffer = new StringBuilder();
								}
								break;

								case '"':
								{
									//
									state = State.QUOTE;
								}
								break;

								case '\'':
								{
									//
									state = State.SIMPLE_QUOTE;
								}
								break;

								default:
								{
									tokenBuffer.append(letter);
								}
							}
						break;

						case QUOTE:
							switch (letter)
							{
								case '\"':
								{
									String token = tokenBuffer.toString();
									tokens.add(token);

									state = State.MAIN;
									tokenBuffer = new StringBuilder();
								}
								break;

								default:
								{
									tokenBuffer.append(letter);
								}
							}
						break;

						case SIMPLE_QUOTE:
							switch (letter)
							{
								case '\'':
								{
									String token = tokenBuffer.toString();
									tokens.add(token);

									state = State.MAIN;
									tokenBuffer = new StringBuilder();
								}
								break;

								default:
								{
									tokenBuffer.append(letter);
								}
							}
						break;
					}

					//
					letterIndex += 1;
				}
				else
				{
					//
					ended = true;

					//
					String token = tokenBuffer.toString();
					if (!StringUtils.isBlank(token))
					{
						tokens.add(token);
					}

					//
					state = State.ENDED;
				}
			}

			//
			result = tokens.toStringArray();
		}

		//
		return result;
	}
}
