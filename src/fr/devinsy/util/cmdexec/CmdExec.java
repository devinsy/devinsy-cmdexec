/*
 *  Copyright (C) 2005-2010,2013,2015-2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-cmdexec.
 * 
 * Devinsy-cmdexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-cmdexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-cmdexec.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.cmdexec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.cmdexec.StreamGobbler.StreamWay;
import fr.devinsy.util.cmdexec.util.CommandSplitter;
import fr.devinsy.util.strings.StringListUtils;

/**
 * The Class CmdExec.
 * 
 * We must use the isOver method on Gobblers because with short tasks the
 * waitFor ends before the Gobbler read.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class CmdExec
{
	private static Logger logger = LoggerFactory.getLogger(CmdExec.class);

	private int exitValue;
	private String out;
	private String err;

	/**
	 * Instantiates a new cmd exec.
	 * 
	 * @param command
	 *            the command
	 */
	public CmdExec(final String command)
	{
		run(command, StreamGobbler.StreamWay.BUFFER, StreamGobbler.StreamWay.BUFFER);
	}

	/**
	 * Instantiates a new cmd exec.
	 * 
	 * @param command
	 *            the command
	 */
	public CmdExec(final String... command)
	{
		run(command, StreamGobbler.StreamWay.BUFFER, StreamGobbler.StreamWay.BUFFER);
	}

	/**
	 * Instantiates a new cmd exec.
	 * 
	 * @param command
	 *            the command
	 * @param outputGobbler
	 *            the output gobbler
	 * @param errorGobbler
	 *            the error gobbler
	 */
	public CmdExec(final String command, final StreamGobbler outputGobbler, final StreamGobbler errorGobbler)
	{
		run(command, outputGobbler, errorGobbler);
	}

	/**
	 * Instantiates a new cmd exec.
	 * 
	 * @param command
	 *            the command
	 * @param stdout
	 *            the stdout
	 * @param stderr
	 *            the stderr
	 */
	public CmdExec(final String command, final StreamGobbler.StreamWay stdout, final StreamGobbler.StreamWay stderr)
	{
		run(command, stdout, stderr);
	}

	/**
	 * Instantiates a new cmd exec.
	 * 
	 * @param command
	 *            the command
	 * @param outputGobbler
	 *            the output gobbler
	 * @param errorGobbler
	 *            the error gobbler
	 */
	public CmdExec(final String[] command, final StreamGobbler outputGobbler, final StreamGobbler errorGobbler)
	{
		run(command, outputGobbler, errorGobbler);
	}

	/**
	 * Instantiates a new cmd exec.
	 * 
	 * @param command
	 *            the command
	 * @param stdout
	 *            the stdout
	 * @param stderr
	 *            the stderr
	 */
	public CmdExec(final String[] command, final StreamGobbler.StreamWay stdout, final StreamGobbler.StreamWay stderr)
	{
		run(command, stdout, stderr);
	}

	// ////////////////////////////////////////////////////////////////////
	//
	// ////////////////////////////////////////////////////////////////////

	/**
	 * Gets the command.
	 * 
	 * @return the command
	 */
	public String getCommand()
	{
		String result;

		// TODO
		result = null;

		//
		return result;
	}

	/**
	 * Gets the err stream.
	 * 
	 * @return the err stream
	 */
	public String getErrStream()
	{
		String result;

		result = this.err;

		//
		return result;
	}

	/**
	 * Gets the exit value.
	 * 
	 * @return the exit value
	 */
	public int getExitValue()
	{
		int result;

		result = this.exitValue;

		//
		return result;
	}

	/**
	 * Gets the out stream.
	 * 
	 * @return the out stream
	 */
	public String getOutStream()
	{
		String result;

		result = this.out;

		//
		return result;
	}

	/**
	 * Run.
	 * 
	 * @param command
	 *            : not a shell command, it must be a executable program.
	 * @param outputGobbler
	 *            the output gobbler
	 * @param errorGobbler
	 *            the error gobbler
	 * @return the int
	 */
	public int run(final String command, final StreamGobbler outputGobbler, final StreamGobbler errorGobbler)
	{
		int result;

		logger.info("CmdExec(command) = [" + command + "]");

		String[] commands = CommandSplitter.split(command);

		result = run(commands, outputGobbler, errorGobbler);

		//
		return result;
	}

	/**
	 * Run.
	 * 
	 * @param command
	 *            the command
	 * @param stdout
	 *            the stdout
	 * @param stderr
	 *            the stderr
	 * @return the int
	 */
	public int run(final String command, final StreamWay stdout, final StreamWay stderr)
	{
		int result;

		result = run(command, new StreamGobbler("OUTPUT", stdout), new StreamGobbler("ERROR", stderr));

		//
		return result;
	}

	/**
	 * Note: this code is inspired by an article of Michael C. Daconta published
	 * in JavaWorld Dec 29, 2000 (http://www.javaworld.com/article/2071275
	 * /core-java/when-runtime-exec---won-t.html?page=2).
	 * 
	 * @param command
	 *            not a shell command, it must be a executable program.
	 * @param outputGobbler
	 *            the output gobbler
	 * @param errorGobbler
	 *            the error gobbler
	 * @return the int
	 */
	public int run(final String[] command, final StreamGobbler outputGobbler, final StreamGobbler errorGobbler)
	{
		int result;

		logger.info("CmdExec(command[]) = [" + StringListUtils.toStringSeparatedBy(command, " ") + "]");
		logger.info("CmdExec(command[]) = [" + StringListUtils.toStringWithBrackets(command) + "]");

		try
		{
			Runtime rt = Runtime.getRuntime();

			Process process = rt.exec(command);

			// Set a collector for error message.
			errorGobbler.setInputStream(process.getErrorStream());

			// Set a collector for output message.
			outputGobbler.setInputStream(process.getInputStream());

			// Collect messages.
			errorGobbler.start();
			outputGobbler.start();

			// Wait and manage the exit value.
			this.exitValue = process.waitFor();
			logger.info("ExitValue: {}", this.exitValue);

			// Sometimes, process ends before Gobblers read its outpout, so we
			// must wait them.
			while ((!outputGobbler.isOver()) || (!errorGobbler.isOver()))
			{
				Thread.sleep(1);
			}

			// Store messages.
			this.out = outputGobbler.getStream();
			this.err = errorGobbler.getStream();

			result = this.exitValue;
		}
		catch (Exception exception)
		{
			this.err = exception.getMessage();
			this.exitValue = -77;
			result = this.exitValue;
			logger.error(exception.getMessage(), exception);
		}

		//
		return result;
	}

	/**
	 * Run.
	 * 
	 * @param command
	 *            the command
	 * @param stdout
	 *            the stdout
	 * @param stderr
	 *            the stderr
	 * @return the int
	 */
	public int run(final String[] command, final StreamGobbler.StreamWay stdout, final StreamGobbler.StreamWay stderr)
	{
		int result;

		result = run(command, new StreamGobbler("OUTPUT", stdout), new StreamGobbler("ERROR", stderr));

		//
		return result;
	}

}