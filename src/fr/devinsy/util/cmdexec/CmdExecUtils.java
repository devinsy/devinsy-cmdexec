/*
 *  Copyright (C) 2005-2010,2013,2015-2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-cmdexec.
 * 
 * Devinsy-cmdexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-cmdexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-cmdexec.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.cmdexec;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.cmdexec.util.CommandSplitter;
import fr.devinsy.util.strings.StringListUtils;

/**
 * The Class CmdExecUtils
 * 
 * We must use the isOver method on Gobblers because with short tasks the
 * waitFor ends before the Gobbler read.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class CmdExecUtils
{
	private static Logger logger = LoggerFactory.getLogger(CmdExecUtils.class);

	// ////////////////////////////////////////////////////////////////////
	//
	// ////////////////////////////////////////////////////////////////////

	/**
	 * Multirun.
	 * 
	 * @param commands
	 *            the commands
	 * @return the string
	 * @throws CmdExecException
	 *             the cmd exec exception
	 */
	public static String multirun(final String... commands) throws CmdExecException
	{
		String result;

		ArrayList<String> stdouts = new ArrayList<String>(commands.length);

		boolean ended = false;
		int commandCounter = 0;
		while (!ended)
		{
			if (commandCounter < commands.length)
			{
				String command = commands[commandCounter];

				if ((command == null) || (command.length() == 0))
				{
					result = null;
					commandCounter += 1;
				}
				else
				{
					stdouts.add(CmdExecUtils.run(command));
					commandCounter += 1;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		int resultLength = 0;
		for (String stdout : stdouts)
		{
			resultLength += stdout.length();
		}
		StringBuffer buffer = new StringBuffer(resultLength);
		for (String stdout : stdouts)
		{
			buffer.append(stdout);
		}
		result = buffer.toString();

		//
		return result;
	}

	// ////////////////////////////////////////////////////////////////////
	//
	// ////////////////////////////////////////////////////////////////////
	/**
	 * Run.
	 * 
	 * @param command
	 *            the command
	 * @return the string
	 * @throws CmdExecException
	 *             the cmd exec exception
	 */
	public static String run(final String command) throws CmdExecException
	{
		String result;

		result = CmdExecUtils.run(CommandSplitter.split(command));

		//
		return result;
	}

	/**
	 * Run.
	 * 
	 * @param command
	 *            the command
	 * @return the string
	 * @throws CmdExecException
	 *             the cmd exec exception
	 */
	public static String run(final String... command) throws CmdExecException
	{
		String result;

		if ((command == null) || (command.length == 0))
		{
			throw new IllegalArgumentException("Empty command");
		}
		else
		{
			CmdExec cmd = new CmdExec(command, StreamGobbler.StreamWay.BUFFER, StreamGobbler.StreamWay.BUFFER);

			if (cmd.getExitValue() == 0)
			{
				result = cmd.getOutStream();
			}
			else
			{
				logger.error("Command=\"{}\"", StringListUtils.toStringWithBrackets(command));
				logger.error("\tout =>  [{}]", StringListUtils.toString(command), cmd.getOutStream());
				logger.error("\terr =>  ({})[{}]", cmd.getErrStream().length(), cmd.getErrStream());
				throw new CmdExecException(cmd.getErrStream());
			}
		}

		//
		return result;
	}

	/**
	 * Examples:
	 * 
	 * run("sudo", "setfacl", "-m", "g:cpm:rwX", "/tmp/toto");
	 * 
	 * run("sudo", "setfacl", "-R", "-m", "g:cpm:rwX", "/tmp/toto");.
	 * 
	 * @param program1
	 *            the program 1
	 * @param program2
	 *            the program 2
	 * @param args
	 *            the args
	 * @param min
	 *            the min
	 * @param max
	 *            the max
	 * @return the string
	 * @throws CmdExecException
	 *             the cmd exec exception
	 */
	public static String run(final String program1, final String program2, final String[] args, final int min, final int max) throws CmdExecException
	{
		String result;

		//
		boolean nullArg = false;
		boolean ended = false;
		int argumentIndex = 0;
		while (!ended)
		{
			if (argumentIndex >= args.length)
			{
				ended = true;
				nullArg = false;
			}
			else
			{
				if (args[argumentIndex] == null)
				{
					ended = true;
					nullArg = true;
				}
				else
				{
					argumentIndex += 1;
				}
			}
		}

		//
		if (program1 == null)
		{
			throw new IllegalArgumentException("Null program parameter 1 detected: [" + program1 + "].");
		}
		else if (program2 == null)
		{
			throw new IllegalArgumentException("Null program parameter 2 detected: [" + program2 + "].");
		}
		else if (nullArg)
		{
			throw new IllegalArgumentException("Null parameter detected in position " + argumentIndex + " for " + StringListUtils.toStringWithBrackets(args) + ".");
		}
		else if ((args.length < min) || (args.length > max))
		{
			throw new IllegalArgumentException("Bad number of parameters: " + args.length + " for " + StringListUtils.toStringWithBrackets(args) + ".");
		}
		else
		{
			//
			String[] command = new String[args.length + 2];
			command[0] = program1;
			command[1] = program2;
			for (argumentIndex = 0; argumentIndex < args.length; argumentIndex++)
			{
				command[argumentIndex + 2] = args[argumentIndex];
			}

			result = CmdExecUtils.run(command);
		}

		//
		return result;
	}

	/**
	 * Examples:
	 * 
	 * run("setfacl", "-m", "g:cpm:rwX", "/tmp/toto");
	 * 
	 * run("setfacl", "-R", "-m", "g:cpm:rwX", "/tmp/toto");.
	 * 
	 * @param program
	 *            the program
	 * @param args
	 *            the args
	 * @param min
	 *            the min
	 * @param max
	 *            the max
	 * @return the string
	 * @throws CmdExecException
	 *             the cmd exec exception
	 */
	public static String run(final String program, final String[] args, final int min, final int max) throws CmdExecException
	{
		String result;

		//
		boolean nullArg = false;
		boolean ended = false;
		int argumentCounter = 0;
		while (!ended)
		{
			if (argumentCounter >= args.length)
			{
				ended = true;
				nullArg = false;
			}
			else
			{
				if (args[argumentCounter] == null)
				{
					ended = true;
					nullArg = true;
				}
				else
				{
					argumentCounter += 1;
				}
			}
		}

		//
		if (program == null)
		{
			throw new IllegalArgumentException("Null program parameter detected: [" + program + "].");
		}
		else if (nullArg)
		{
			throw new IllegalArgumentException("Null parameter detected in position " + argumentCounter + " for " + StringListUtils.toStringWithBrackets(args) + ".");
		}
		else if ((args.length < min) || (args.length > max))
		{
			throw new IllegalArgumentException("Bad number of parameters: " + args.length + " for " + StringListUtils.toStringWithBrackets(args) + ".");
		}
		else
		{
			//
			String[] command = new String[args.length + 1];
			command[0] = program;
			for (argumentCounter = 0; argumentCounter < args.length; argumentCounter++)
			{
				command[argumentCounter + 1] = args[argumentCounter];
			}

			result = CmdExecUtils.run(command);
		}

		//
		return result;
	}
}
