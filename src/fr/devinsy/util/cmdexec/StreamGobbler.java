/*
 * Copyright (C) 2005-2008,2010,2013,2016-2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-cmdexec.
 * 
 * Devinsy-cmdexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-cmdexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-cmdexec.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.cmdexec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class StreamGobbler.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class StreamGobbler extends Thread
{
	public enum StreamWay
	{
		NONE,
		PRINT,
		BUFFER
	}

	private static final Logger logger = LoggerFactory.getLogger(CmdExec.class);

	private InputStream is;
	private String name;
	private StreamWay streamWay;
	private StringBuffer stream;

	// Important if the caller wants have complete stream in case of very short
	// command.
	private boolean isOverStatus;

	/**
	 * Instantiates a new stream gobbler.
	 */
	StreamGobbler()
	{
		this.is = null;
		this.name = "";
		this.streamWay = StreamWay.NONE;
		this.stream = new StringBuffer();
		this.isOverStatus = false;
	}

	/**
	 * Instantiates a new stream gobbler.
	 * 
	 * @param is
	 *            the is
	 * @param name
	 *            the name
	 */
	StreamGobbler(final InputStream is, final String name)
	{
		this.is = is;
		this.name = name;
		this.streamWay = StreamWay.NONE;
		this.stream = new StringBuffer();
		this.isOverStatus = false;
	}

	/**
	 * Instantiates a new stream gobbler.
	 * 
	 * @param is
	 *            the is
	 * @param name
	 *            the name
	 * @param streamWay
	 *            the stream way
	 */
	StreamGobbler(final InputStream is, final String name, final StreamWay streamWay)
	{
		this.is = is;
		this.name = name;
		this.streamWay = streamWay;
		this.stream = new StringBuffer();
		this.isOverStatus = false;
	}

	/**
	 * Instantiates a new stream gobbler.
	 * 
	 * @param type
	 *            the type
	 * @param streamWay
	 *            the stream way
	 */
	StreamGobbler(final String type, final StreamWay streamWay)
	{
		this.name = type;
		this.streamWay = streamWay;
		this.stream = new StringBuffer();
		this.isOverStatus = false;
	}

	/**
	 * Gets the stream.
	 * 
	 * @return the stream
	 */
	public String getStream()
	{
		String result;

		if (this.stream != null)
		{
			result = this.stream.toString();
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Checks if is over.
	 * 
	 * @return true, if is over
	 */
	public boolean isOver()
	{
		boolean result;

		result = this.isOverStatus;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run()
	{
		try
		{
			InputStreamReader isr = new InputStreamReader(this.is);
			BufferedReader buffer = new BufferedReader(isr);
			String line = null;
			switch (this.streamWay)
			{
				case NONE:
					while ((line = buffer.readLine()) != null)
					{
						;
					}
				break;

				case PRINT:
					while ((line = buffer.readLine()) != null)
					{
						System.out.println(this.name + ">" + line);
					}
				break;

				case BUFFER:
					while ((line = buffer.readLine()) != null)
					{
						this.stream.append(line + "\n");
					}
				break;

				default:
					logger.warn("unknow way for stream");
			}
		}
		catch (IOException exception)
		{
			logger.error(exception.getMessage(), exception);
		}

		this.isOverStatus = true;
	}

	/**
	 * Sets the input stream.
	 * 
	 * @param is
	 *            the new input stream
	 */
	public void setInputStream(final InputStream is)
	{
		this.is = is;
	}
}
