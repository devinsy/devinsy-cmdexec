/*
 * Copyright (C) 2013,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-cmdexec.
 * 
 * Devinsy-cmdexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-cmdexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-cmdexec.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.cmdexec;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.PatternLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CmdExecSandbox.
 */
class CmdExecSandbox
{
	static private final Logger logger;

	static
	{
		// Initialize logger.
		org.apache.log4j.BasicConfigurator.configure();
		org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.INFO);

		logger = LoggerFactory.getLogger(CmdExecSandbox.class);
		logger.info("Enter");

		//
		logger.info("Set the log file format…");
		org.apache.log4j.Logger defaultLogger = org.apache.log4j.Logger.getRootLogger();
		defaultLogger.removeAllAppenders();
		defaultLogger.addAppender(new ConsoleAppender(new PatternLayout("%d{ISO8601} - dutils [%-5p] %34.34c.%-25M - %m%n")));

		logger.info("… done.");

		logger.debug("Exit");
	}

	/**
	 * Check.
	 * 
	 * @param title
	 *            the title
	 * @param source
	 *            the source
	 * @param model
	 *            the model
	 * @return the string
	 */
	public static String check(final String title, final StringBuffer source, final String model)
	{
		String result;

		if (source.indexOf(model) == -1)
		{
			result = String.format("%-40s  ->  KO  <-", title) + "\nGet:\n" + source + "\nWaiting:\n" + model;

		}
		else
		{
			result = String.format("%-40s    [ OK ]  ", title);
		}

		//
		return result;
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		System.out.println("Automatic test action for CmdExec!");

		test1();

	}

	/**
	 * Test 1.
	 */
	public static void test1()
	{
		try
		{
			System.out.println("Launch…");

			// String command = "/bin/sort -r /etc/passwd";
			String[] command = { "/usr/bin/sort", "-r", "/etc/passwd" };

			CmdExec cmd = new CmdExec(command, StreamGobbler.StreamWay.BUFFER, StreamGobbler.StreamWay.BUFFER);
			System.out.println("exitVal=[" + cmd.getExitValue() + "]");
			System.out.println("out=[" + cmd.getOutStream() + "]");
			System.out.println("err=[" + cmd.getErrStream() + "]");
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			logger.info("ERRRO=" + exception);
		}
	}
}
