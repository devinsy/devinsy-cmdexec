/*
 * Copyright (C) 2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-cmdexec.
 * 
 * Devinsy-cmdexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-cmdexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-cmdexec.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.cmdexec.util;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.strings.StringList;

/**
 * The Class SplitWorkerTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class CommandSplitterTest
{
	private static Logger logger = LoggerFactory.getLogger(CommandSplitterTest.class);

	/**
	 * After.
	 */
	@After
	public void after()
	{
	}

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		org.apache.log4j.Logger.getRootLogger().setLevel(Level.INFO);
	}

	/**
	 * Test 01.
	 */
	@Test
	public void testt01()
	{
		//
		this.logger.debug("===== test starting...");

		String[] tokens = CommandSplitter.split("bash -c \"to't o\"   lala 'ti\" t i' lulu");

		logger.info(new StringList(tokens).toStringWithBrackets());

		//
		logger.debug("===== test done.");
	}
}
